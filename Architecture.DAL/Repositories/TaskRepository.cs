using System.Linq;
using System.Collections.Generic;
using Architecture.DAL.Entities;
using Architecture.Common.Exceptions;

namespace Architecture.DAL.Repositories
{
    public sealed class TaskRepository : BaseRepository<TaskModel>
    {
        private readonly IRepository<User> _userRepository;
        protected override List<TaskModel> Data { get => _context.Tasks; set => _context.Tasks = value; }

        public TaskRepository(DBContext context, IRepository<User> userRepository) : base(context)
        {
            _userRepository = userRepository;
        }

        public override TaskModel Format(TaskModel data)
        {
            data.Performer = _userRepository.Get(data.PerformerId);

            return data;
        }

        public override IEnumerable<TaskModel> Format(IEnumerable<TaskModel> data)
        {
            return data.Join<TaskModel, User, int, TaskModel>(
                _userRepository.Get(),
                t => t.PerformerId,
                u => u.Id,
                (task, user) => new TaskModel
                {
                    Id = task.Id,
                    PerformerId = task.PerformerId,
                    ProjectId = task.ProjectId,
                    Name = task.Name,
                    Description = task.Description,
                    State = task.State,
                    CreatedAt = task.CreatedAt,
                    FinishedAt = task.FinishedAt,
                    Performer = user,
                }
            );
        }

        public override void CheckRelations(TaskModel data)
        {
            if (!_context.Projects.Exists(p => p.Id == data.ProjectId))
            {
                throw new NotFoundException("Project");
            }

            if (!_context.Users.Exists(u => u.Id == data.PerformerId))
            {
                throw new NotFoundException("Performer");
            }
        }
    }
}
