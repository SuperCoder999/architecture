using System.Collections.Generic;
using Architecture.DAL.Entities;
using Architecture.Common.Exceptions;

namespace Architecture.DAL.Repositories
{
    public sealed class UserRepository : BaseRepository<User>
    {
        protected override List<User> Data { get => _context.Users; set => _context.Users = value; }

        public UserRepository(DBContext context) : base(context) { }

        public override void CheckRelations(User data)
        {
            if (!_context.Teams.Exists(t => t.Id == data.TeamId))
            {
                throw new NotFoundException("Team");
            }
        }
    }
}
