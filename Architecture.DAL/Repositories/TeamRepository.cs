using System.Linq;
using System.Collections.Generic;
using Architecture.DAL.Entities;

namespace Architecture.DAL.Repositories
{
    public sealed class TeamRepository : BaseRepository<Team>
    {
        private readonly IRepository<User> _userRepository;
        protected override List<Team> Data { get => _context.Teams; set => _context.Teams = value; }

        public TeamRepository(DBContext context, IRepository<User> userRepository) : base(context)
        {
            _userRepository = userRepository;
        }

        public override Team Format(Team data)
        {
            IEnumerable<User> users = _userRepository.Get().Where(user => user.TeamId == data.Id);
            data.Users = users;

            return data;
        }

        public override IEnumerable<Team> Format(IEnumerable<Team> data)
        {
            return data.GroupJoin<Team, User, int, Team>(
                _userRepository.Get().Where(user => user.TeamId.HasValue),
                t => t.Id,
                u => u.TeamId.Value,
                (team, users) => new Team
                {
                    Id = team.Id,
                    Name = team.Name,
                    CreatedAt = team.CreatedAt,
                    Users = users,
                }
            );
        }
    }
}
