using System.Linq;
using System.Collections.Generic;
using Architecture.DAL.Entities;
using Architecture.Common.Exceptions;

namespace Architecture.DAL.Repositories
{
    public sealed class ProjectRepository : BaseRepository<Project>
    {
        private readonly IRepository<TaskModel> _taskRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<User> _userRepository;
        protected override List<Project> Data { get => _context.Projects; set => _context.Projects = value; }

        public ProjectRepository(
            DBContext context,
            IRepository<TaskModel> taskRepository,
            IRepository<Team> teamRepository,
            IRepository<User> userRepository
        ) : base(context)
        {
            _taskRepository = taskRepository;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
        }

        public override Project Format(Project data)
        {
            data.Tasks = _taskRepository.Get().Where(task => task.ProjectId == data.Id);
            data.Author = _userRepository.Get(data.AuthorId);
            data.Team = _teamRepository.Get(data.TeamId);

            return data;
        }

        public override IEnumerable<Project> Format(IEnumerable<Project> data)
        {
            return data
                .Join<Project, User, int, Project>(
                    _userRepository.Get(),
                    p => p.AuthorId,
                    u => u.Id,
                    (project, author) => new Project
                    {
                        Id = project.Id,
                        AuthorId = project.AuthorId,
                        TeamId = project.TeamId,
                        Name = project.Name,
                        Description = project.Description,
                        Deadline = project.Deadline,
                        CreatedAt = project.CreatedAt,
                        Author = author,
                    }
                )
                .Join<Project, Team, int, Project>(
                    _teamRepository.Get(),
                    p => p.TeamId,
                    t => t.Id,
                    (project, team) => new Project
                    {
                        Id = project.Id,
                        AuthorId = project.AuthorId,
                        TeamId = project.TeamId,
                        Name = project.Name,
                        Description = project.Description,
                        Deadline = project.Deadline,
                        CreatedAt = project.CreatedAt,
                        Author = project.Author,
                        Team = project.Team,
                    }
                )
                .GroupJoin<Project, TaskModel, int, Project>(
                    _taskRepository.Get(),
                    p => p.Id,
                    t => t.ProjectId,
                    (project, tasks) => new Project
                    {
                        Id = project.Id,
                        AuthorId = project.AuthorId,
                        TeamId = project.TeamId,
                        Name = project.Name,
                        Description = project.Description,
                        Deadline = project.Deadline,
                        CreatedAt = project.CreatedAt,
                        Author = project.Author,
                        Team = project.Team,
                        Tasks = tasks,
                    }
                );
        }

        public override void CheckRelations(Project data)
        {
            if (!_context.Users.Exists(u => u.Id == data.AuthorId))
            {
                throw new NotFoundException("Author");
            }

            if (!_context.Teams.Exists(t => t.Id == data.TeamId))
            {
                throw new NotFoundException("Team");
            }
        }
    }
}
