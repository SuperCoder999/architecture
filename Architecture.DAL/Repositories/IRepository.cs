using System.Collections.Generic;
using Architecture.DAL.Entities;

namespace Architecture.DAL.Repositories
{
    public interface IRepository<T> where T : IEntity
    {
        IEnumerable<T> Get();
        T Get(int id);
        bool Exists(int id);
        T Create(T data);
        T Update(int id, T data);
        void Delete(int id);
        int NextId();
        T Format(T data);
        IEnumerable<T> Format(IEnumerable<T> data);
        void CheckRelations(T data);
    }
}
