using System.Collections.Generic;
using System.Linq;
using Architecture.DAL.Entities;
using Architecture.Common.CommonExtensions;
using Architecture.Common.Exceptions;

namespace Architecture.DAL.Repositories
{
    public abstract class BaseRepository<T> : IRepository<T> where T : IEntity
    {
        protected readonly DBContext _context;
        protected abstract List<T> Data { get; set; }

        public BaseRepository(DBContext context)
        {
            _context = context;
        }

        public virtual IEnumerable<T> Get()
        {
            return Format(Data.ToList());
        }

        public virtual T Get(int id)
        {
            if (!Exists(id))
            {
                throw new NotFoundException();
            }

            return Format(Data.Find(item => item.Id == id));
        }

        public virtual bool Exists(int id)
        {
            return Data.Exists(item => item.Id == id);
        }

        public virtual T Create(T data)
        {
            CheckRelations(data);
            data.Id = NextId(); // It is NOT mapping. It is id generating.
            Data.Add(data);

            return Format(data);
        }

        public virtual T Update(int id, T data)
        {
            if (!Exists(id))
            {
                throw new NotFoundException();
            }

            CheckRelations(data);
            Data = Data.ReplaceWhere<T, int>(item => item.Id == id, data).ToList();
            return Format(data);
        }

        public virtual void Delete(int id)
        {
            if (!Exists(id))
            {
                throw new NotFoundException();
            }

            Data = Data.RemoveWhere<T, int>(item => item.Id == id).ToList();
        }

        public virtual int NextId()
        {
            return Data.Select(item => item.Id).OrderByDescending(item => item).FirstOrDefault() + 1;
        }

        public virtual T Format(T data) => data;
        public virtual IEnumerable<T> Format(IEnumerable<T> data) => data;
        public virtual void CheckRelations(T data) { }
    }
}
