﻿using System.Collections.Generic;
using Architecture.DAL.Entities;

namespace Architecture.DAL
{
    public class DBContext
    {
        public List<User> Users = new List<User>();
        public List<TaskModel> Tasks = new List<TaskModel>();
        public List<Team> Teams = new List<Team>();
        public List<Project> Projects = new List<Project>();
    }
}
