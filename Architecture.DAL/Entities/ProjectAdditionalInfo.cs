#nullable enable

namespace Architecture.DAL.Entities
{
    public struct ProjectAdditionalInfo
    {
        public Project Project { get; set; }
        public TaskModel LongestDescriptionTask { get; set; }
        public TaskModel ShortestNameTask { get; set; }
        public int? UsersCountInTeam { get; set; }
    }
}
