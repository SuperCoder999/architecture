using System;
using System.Collections.Generic;

namespace Architecture.DAL.Entities
{
    public struct Team : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<User> Users;
    }
}
