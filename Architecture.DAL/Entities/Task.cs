#nullable enable

using System;
using Architecture.Common.Enums;

namespace Architecture.DAL.Entities
{
    public struct TaskModel : IEntity // Conflicts with System.Threading.Tasks.Task
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public User Performer { get; set; }
    }
}
