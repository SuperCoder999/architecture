#nullable enable

namespace Architecture.DAL.Entities
{
    public struct UserAdditionalInfo
    {
        public User User { get; set; }
        public Project? LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public TaskModel? LongestTask { get; set; }
    }
}
