#nullable enable

using System;
using System.Collections.Generic;

namespace Architecture.DAL.Entities
{
    public struct User : IEntity
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public IEnumerable<TaskModel> Tasks { get; set; }
    }
}
