using System;
using System.Net;
using Architecture.Common.Exceptions;

namespace Architecture.WebAPI.Extensions
{
    public static class ExceptionFilterExtensions
    {
        public static HttpStatusCode ParseException(this Exception exception)
        {
            return exception switch
            {
                NotFoundException _ => HttpStatusCode.NotFound,
                _ => HttpStatusCode.InternalServerError,
            };
        }
    }
}
