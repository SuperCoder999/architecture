using Microsoft.Extensions.DependencyInjection;
using FluentValidation;
using FluentValidation.AspNetCore;
using Architecture.DAL;
using Architecture.DAL.Entities;
using Architecture.DAL.Repositories;
using Architecture.BLL.Sevices;
using Architecture.BLL.Interfaces;
using Architecture.BLL.MappingProfiles;
using Architecture.BLL.Validators.User;
using Architecture.BLL.Validators.Task;
using Architecture.BLL.Validators.Team;
using Architecture.BLL.Validators.Project;
using Architecture.Common.DTO.User;
using Architecture.Common.DTO.Task;
using Architecture.Common.DTO.Team;
using Architecture.Common.DTO.Project;
using Architecture.WebAPI.Filters;
using Microsoft.OpenApi.Models;

namespace Architecture.WebAPI.Extensions
{
    public static class ServiceExtenions
    {
        public static void AddDBContext(this IServiceCollection services)
        {
            services.AddSingleton<DBContext>();
        }

        public static void AddMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(opt =>
            {
                opt.AddProfile<UserProfile>();
                opt.AddProfile<TaskProfile>();
                opt.AddProfile<TeamProfile>();
                opt.AddProfile<ProjectProfile>();
            });
        }

        public static void AddValidation(this IServiceCollection services)
        {
            services.AddSingleton<IValidator<CreateUserDTO>, CreateUserValidator>();
            services.AddSingleton<IValidator<UpdateUserDTO>, UpdateUserValidator>();

            services.AddSingleton<IValidator<CreateTaskDTO>, CreateTaskValidator>();
            services.AddSingleton<IValidator<UpdateTaskDTO>, UpdateTaskValidator>();

            services.AddSingleton<IValidator<CreateTeamDTO>, CreateTeamValidator>();
            services.AddSingleton<IValidator<UpdateTeamDTO>, UpdateTeamValidator>();

            services.AddSingleton<IValidator<CreateProjectDTO>, CreateProjectValidator>();
            services.AddSingleton<IValidator<UpdateProjectDTO>, UpdateProjectValidator>();
        }

        public static void AddDAL(this IServiceCollection services)
        {
            services.AddSingleton<IRepository<User>, UserRepository>();
            services.AddSingleton<IRepository<TaskModel>, TaskRepository>();
            services.AddSingleton<IRepository<Team>, TeamRepository>();
            services.AddSingleton<IRepository<Project>, ProjectRepository>();
        }

        public static void AddBLL(this IServiceCollection services)
        {
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<ITaskService, TaskService>();
            services.AddSingleton<ITeamService, TeamService>();
            services.AddSingleton<IProjectService, ProjectService>();
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Architecture API",
                    Version = "1.0.0"
                });
            });
        }

        public static void AddOtherModules(this IServiceCollection services)
        {
            services
                .AddControllers(opt => opt.Filters.Add(typeof(CustomExceptionFilterAttribute)))
                .AddNewtonsoftJson()
                .AddFluentValidation();

            services.AddCors();
        }
    }
}
