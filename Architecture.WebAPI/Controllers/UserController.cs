using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;
using Architecture.BLL.Interfaces;
using Architecture.Common.DTO.User;

namespace Architecture.WebAPI.Controllers
{
    [ApiController]
    [Route("api/users")]
    public sealed class UserController : AbstractController<UserDTO, CreateUserDTO, UpdateUserDTO, IUserService>
    {
        public UserController(
            IUserService service,
            IValidator<CreateUserDTO> createValidator,
            IValidator<UpdateUserDTO> updateValidator
        ) : base(service, createValidator, updateValidator) { }

        [HttpGet("with-tasks")]
        public ActionResult<IEnumerable<UserWithTasksDTO>> GetWithTasksOrderedByFirstName()
        {
            return Ok(_service.GetWithTasksOrderedByFirstName());
        }

        [HttpGet("{id}/additional-info")]
        public ActionResult<UserAdditionalInfoDTO> GetAdditionalInfo([FromRoute] int id)
        {
            return Ok(_service.GetAdditionalInfo(id));
        }
    }
}
