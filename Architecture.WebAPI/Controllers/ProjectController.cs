using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;
using Architecture.BLL.Interfaces;
using Architecture.Common.DTO.Project;

namespace Architecture.WebAPI.Controllers
{
    [ApiController]
    [Route("api/projects")]
    public sealed class ProjectController : AbstractController<ProjectDTO, CreateProjectDTO, UpdateProjectDTO, IProjectService>
    {
        public ProjectController(
            IProjectService service,
            IValidator<CreateProjectDTO> createValidator,
            IValidator<UpdateProjectDTO> updateValidator
        ) : base(service, createValidator, updateValidator) { }

        [HttpGet("with-tasks-count/{userId}")]
        public ActionResult<IEnumerable<ProjectWithTasksCountDTO>> GetWithTasksCount([FromRoute] int userId)
        {
            return Ok(_service.WithTasksCountByUserId(userId));
        }

        [HttpGet("{id}/additional-info")]
        public ActionResult<ProjectAdditionalInfoDTO> GetAdditionalInfo([FromRoute] int id)
        {
            return Ok(_service.GetAdditionalInfo(id));
        }
    }
}
