using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;
using Architecture.BLL.Interfaces;
using Architecture.Common.DTO.Task;

namespace Architecture.WebAPI.Controllers
{
    [ApiController]
    [Route("api/tasks")]
    public sealed class TaskController : AbstractController<TaskDTO, CreateTaskDTO, UpdateTaskDTO, ITaskService>
    {
        public TaskController(
            ITaskService service,
            IValidator<CreateTaskDTO> createValidator,
            IValidator<UpdateTaskDTO> updateValidator
        ) : base(service, createValidator, updateValidator) { }

        [HttpGet("with-short-name/{userId}")]
        public ActionResult<IEnumerable<TaskDTO>> GetAssignedToWithShortName([FromRoute] int userId)
        {
            return Ok(_service.GetAssignedToWithShortName(userId));
        }

        [HttpGet("finished-in-current-year/{userId}")]
        public ActionResult<IEnumerable<TaskShortDTO>> GetShortFinishedInCurrentYearAssignedTo([FromRoute] int userId)
        {
            return Ok(_service.GetShortFinishedInCurrentYearAssignedTo(userId));
        }
    }
}
