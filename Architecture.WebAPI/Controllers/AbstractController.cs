using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;
using Architecture.BLL.Interfaces;

namespace Architecture.WebAPI.Controllers
{
    public abstract class AbstractController<TDTO, TCreateDTO, TUpdateDTO, TService> : ControllerBase
        where TDTO : struct
        where TCreateDTO : struct
        where TUpdateDTO : struct
        where TService : IServiceBase<TDTO, TCreateDTO, TUpdateDTO>
    {
        protected readonly TService _service;
        protected readonly IValidator<TCreateDTO> _createValidator;
        protected readonly IValidator<TUpdateDTO> _updateValidator;

        public AbstractController(
            TService service,
            IValidator<TCreateDTO> createValidator,
            IValidator<TUpdateDTO> updateValidator
        )
        {
            _service = service;
            _createValidator = createValidator;
            _updateValidator = updateValidator;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TDTO>> Get()
        {
            return Ok(_service.Get());
        }

        [HttpGet("{id}")]
        public ActionResult<TDTO> Get([FromRoute] int id)
        {
            return Ok(_service.Get(id));
        }

        [HttpPost]
        public ActionResult<TDTO> Create([FromBody] TCreateDTO data)
        {
            _createValidator.ValidateAndThrow(data);
            return Created(nameof(Create), _service.Create(data));
        }

        [HttpPatch("{id}")]
        public ActionResult<TDTO> Update([FromRoute] int id, [FromBody] TUpdateDTO data)
        {
            _updateValidator.ValidateAndThrow(data);
            return Ok(_service.Update(id, data));
        }

        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] int id)
        {
            _service.Delete(id);
            return NoContent();
        }
    }
}
