using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;
using Architecture.BLL.Interfaces;
using Architecture.Common.DTO.Team;

namespace Architecture.WebAPI.Controllers
{
    [ApiController]
    [Route("api/teams")]
    public sealed class TeamController : AbstractController<TeamDTO, CreateTeamDTO, UpdateTeamDTO, ITeamService>
    {
        public TeamController(
            ITeamService service,
            IValidator<CreateTeamDTO> createValidator,
            IValidator<UpdateTeamDTO> updateValidator
        ) : base(service, createValidator, updateValidator) { }

        [HttpGet("ids-names-users")]
        public ActionResult<IEnumerable<TeamShortDTO>> GetIdsNamesUsers()
        {
            return Ok(_service.GetIdsNamesUsers());
        }
    }
}
