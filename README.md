# Architecture

## Run:

### Server:

- `cd Architecture.WebAPI`
- `dotnet run`

### Client:

- `cd Architecture.Client`
- `dotnet run`

## Important!

- There is a bug: if we set nullable field to null during update, it is not updated. This bug appears, because I intersect update DTO and existing entity and I cannot determine, was field present in JSON or not. If you have any recommendations about fixing this, please write. Thanks.

## Extra services

- Fluent Validation (`Architecture.BLL/Validators`)
- Auto Mapper (`Architecture.BLL/MappingProfiles`)
- Swagger UI (`Architecture.WebAPI/Startup.cs`)

## Notes:

- Data Access Layer is located at `Architecture.DAL`
- Business Logic Level is located at `Architecture.BLL`
- Controllers are located at `Architecture.WebAPI`
- Swagger endpoint - [http://localhost:5000/swagger](http://localhost:5000/swagger)
- Update request uses PATCH method
