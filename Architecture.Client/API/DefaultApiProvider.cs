#nullable enable

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using Architecture.Common;
using Architecture.Common.DTO;

namespace Architecture.Client.API
{
    internal sealed class DefaultApiProvider : IApiProvider
    {
        private static readonly string BaseUrl = "http://localhost:5000/api/";
        private readonly HttpClient client = new HttpClient();

        public DefaultApiProvider()
        {
            client.BaseAddress = new Uri(BaseUrl);
        }

        public async Task<T> Get<T>(string endpoint)
        {
            HttpResponseMessage response = await client.GetAsync(endpoint);
            string stringBody = await response.Content.ReadAsStringAsync();
            ThrowIfResponseNotOk(response, stringBody);
            T result = Utils.DeserializeJson<T>(stringBody);

            return result;
        }

        public void Dispose()
        {
            client.Dispose();
        }

        private void ThrowIfResponseNotOk(HttpResponseMessage response, string strBody)
        {
            if (!response.IsSuccessStatusCode)
            {
                HttpErrorDTO? error = Utils.DeserializeJson<HttpErrorDTO>(strBody);

                if (error.HasValue && error.Value.Errors.Count > 0)
                {
                    IEnumerable<string> errors = error.Value.Errors.SelectMany(pair => pair.Value);
                    throw new InvalidOperationException(string.Join(", ", errors));
                }

                throw new InvalidOperationException(response.ReasonPhrase);
            }
        }
    }
}
