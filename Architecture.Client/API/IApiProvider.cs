using System;
using System.Threading.Tasks;

namespace Architecture.Client.API
{
    public interface IApiProvider : IDisposable
    {
        Task<T> Get<T>(string endpoint);
    }
}
