using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Architecture.Common.DTO.User;
using Architecture.Common.DTO.Task;
using Architecture.Client.Services;

namespace Architecture.Client.Commands
{
    [Command(5, "getUsersOrderedByFirstNameWithTasks")]
    internal sealed class GetUsersOrderedByFirstNameWithTasks
        : BaseCommand<GetUsersOrderedByFirstNameWithTasks>
    {
        private readonly UserService userService = new UserService();

        public override async Task Invoke()
        {
            IEnumerable<UserWithTasksDTO> users = await userService.GetWithTasksOrderedByFirstName();

            foreach (UserWithTasksDTO user in users)
            {
                Console.WriteLine($"{user.FirstName} {user.LastName}:");

                foreach (TaskWithoutPerformerDTO task in user.Tasks)
                {
                    Console.WriteLine($"  {task.Name}");
                }

                Console.WriteLine();
            }
        }
    }
}
