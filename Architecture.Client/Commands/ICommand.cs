using System.Threading.Tasks;

namespace Architecture.Client.Commands
{
    internal interface ICommand
    {
        int Index { get; }
        Task Invoke();
        void Reset();
        string ToString();
    }
}
