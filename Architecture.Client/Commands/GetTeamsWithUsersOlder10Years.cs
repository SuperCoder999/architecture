using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Architecture.Common.DTO.User;
using Architecture.Common.DTO.Team;
using Architecture.Client.Services;

namespace Architecture.Client.Commands
{
    [Command(4, "getTeamsWithUsersOlder10Years")]
    internal sealed class GetTeamsWithUsersOlder10Years : BaseCommand<GetTeamsWithUsersOlder10Years>
    {
        private readonly TeamService teamService = new TeamService();

        public override async Task Invoke()
        {
            IEnumerable<TeamShortDTO> data = await teamService.GetIdsNamesUsers();

            foreach (TeamShortDTO dto in data)
            {
                Console.WriteLine($"Id: {dto.Id}, Name: {dto.Name}, Users:");

                foreach (UserDTO user in dto.Users)
                {
                    Console.WriteLine($"  {user.FirstName} {user.LastName}");
                }

                Console.WriteLine();
            }
        }
    }
}
