using Architecture.Client.API;

namespace Architecture.Client.Services
{
    internal abstract class AbstarctService
    {
        protected readonly ApiClient _api;

        public AbstarctService(string endpoint)
        {
            _api = new ApiClient(new DefaultApiProvider(), endpoint);
        }
    }
}
