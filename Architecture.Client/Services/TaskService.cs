using System.Threading.Tasks;
using System.Collections.Generic;
using Architecture.Common.DTO.Task;

namespace Architecture.Client.Services
{
    internal sealed class TaskService : AbstarctService
    {
        public TaskService() : base("tasks") { }

        public async Task<IEnumerable<TaskDTO>> GetAssignedToWithShortName(int userId)
        {
            return await _api.Get<IEnumerable<TaskDTO>>($"with-short-name/{userId}");
        }

        public async Task<IEnumerable<TaskShortDTO>> GetShortFinishedInCurrentYearAssignedTo(int userId)
        {
            return await _api.Get<IEnumerable<TaskShortDTO>>($"finished-in-current-year/{userId}");
        }
    }
}
