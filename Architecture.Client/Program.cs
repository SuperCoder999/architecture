﻿using System.Threading.Tasks;
using Architecture.Client.ConsoleInterface;

namespace Architecture.Client
{
    public sealed class Program
    {
        public static async Task Main(string[] args)
        {
            ConsoleMenu app = new ConsoleMenu();
            await app.Run();
        }
    }
}
