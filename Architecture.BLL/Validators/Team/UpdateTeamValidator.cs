using FluentValidation;
using Architecture.Common.DTO.Team;

namespace Architecture.BLL.Validators.Team
{
    public sealed class UpdateTeamValidator : AbstractValidator<UpdateTeamDTO>
    {
        public UpdateTeamValidator()
        {
            When(
                t => t.Name != null,
                () => RuleFor(t => t.Name)
                    .NotEmpty()
                    .WithMessage("Name must not be empty")
            );
        }
    }
}
