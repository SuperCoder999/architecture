using FluentValidation;
using Architecture.Common.DTO.Team;

namespace Architecture.BLL.Validators.Team
{
    public sealed class CreateTeamValidator : AbstractValidator<CreateTeamDTO>
    {
        public CreateTeamValidator()
        {
            RuleFor(t => t.Name)
                .NotEmpty()
                .WithMessage("Name must not be empty");
        }
    }
}
