using FluentValidation;
using Architecture.Common.DTO.Project;

namespace Architecture.BLL.Validators.Project
{
    public sealed class CreateProjectValidator : AbstractValidator<CreateProjectDTO>
    {
        public CreateProjectValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .WithMessage("Name must not be empty");

            RuleFor(p => p.Description)
                .NotEmpty()
                .WithMessage("Description must not be empty");

            RuleFor(p => p.Deadline)
                .NotEmpty()
                .WithMessage("Deadline must not be empty");
        }
    }
}
