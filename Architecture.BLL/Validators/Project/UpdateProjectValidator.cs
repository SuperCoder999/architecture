using FluentValidation;
using Architecture.Common.DTO.Project;

namespace Architecture.BLL.Validators.Project
{
    public sealed class UpdateProjectValidator : AbstractValidator<UpdateProjectDTO>
    {
        public UpdateProjectValidator()
        {
            When(
                p => p.Name != null,
                () => RuleFor(p => p.Name)
                    .NotEmpty()
                    .WithMessage("Name must not be empty")
            );

            When(
                p => p.Description != null,
                () => RuleFor(p => p.Description)
                    .NotEmpty()
                    .WithMessage("Description must not be empty")
            );

            When(
                p => p.Deadline != null,
                () => RuleFor(p => p.Deadline)
                    .NotEmpty()
                    .WithMessage("Deadline must not be empty")
            );
        }
    }
}
