using System;
using FluentValidation;
using Architecture.Common.DTO.Task;

namespace Architecture.BLL.Validators.Task
{
    public sealed class UpdateTaskValidator : AbstractValidator<UpdateTaskDTO>
    {
        public UpdateTaskValidator()
        {
            When(
                t => t.Name != null,
                () => RuleFor(t => t.Name)
                    .NotEmpty()
                    .WithMessage("Name must not be empty")
            );

            When(
                t => t.Description != null,
                () => RuleFor(t => t.Description)
                    .NotEmpty()
                    .WithMessage("Description must not me empty")
            );

            When(
                t => t.State != null,
                () => RuleFor(t => t.State)
                    .NotEmpty()
                    .IsInEnum()
                    .WithMessage("State must be from 0 to 3")
            );

            When(
                t => t.FinishedAt != null,
                () => RuleFor(t => t.FinishedAt)
                    .NotEmpty()
                    .LessThan(DateTime.Now)
                    .WithMessage("Finish date must be earlier than now")
            );
        }
    }
}
