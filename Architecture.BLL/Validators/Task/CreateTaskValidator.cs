using System;
using FluentValidation;
using Architecture.Common.DTO.Task;

namespace Architecture.BLL.Validators.Task
{
    public sealed class CreateTaskValidator : AbstractValidator<CreateTaskDTO>
    {
        public CreateTaskValidator()
        {
            RuleFor(t => t.Name)
                .NotEmpty()
                .WithMessage("Name must not be empty");

            RuleFor(t => t.Description)
                .NotEmpty()
                .WithMessage("Description must not me empty");

            RuleFor(t => t.State)
                .NotEmpty()
                .IsInEnum()
                .WithMessage("State must be from 0 to 3");

            When(
                t => t.FinishedAt != null,
                () => RuleFor(t => t.FinishedAt)
                    .NotEmpty()
                    .LessThan(DateTime.Now)
                    .WithMessage("Finish date must be earlier than now")
            );
        }
    }
}
