using System;
using FluentValidation;
using Architecture.Common.DTO.User;

namespace Architecture.BLL.Validators.User
{
    public sealed class CreateUserValidator : AbstractValidator<CreateUserDTO>
    {
        public CreateUserValidator()
        {
            RuleFor(u => u.Email)
                .EmailAddress()
                .WithMessage("Email address must be valid");

            RuleFor(u => u.FirstName)
                .NotEmpty()
                .WithMessage("First name must not be empty");

            RuleFor(u => u.LastName)
                .NotEmpty()
                .WithMessage("Last name must not be empty");

            RuleFor(u => u.BirthDay)
                .NotEmpty()
                .LessThan(DateTime.UtcNow)
                .WithMessage("Birth day must be earlier than now");
        }
    }
}
