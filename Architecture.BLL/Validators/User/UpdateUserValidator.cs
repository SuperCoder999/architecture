using System;
using FluentValidation;
using Architecture.Common.DTO.User;

namespace Architecture.BLL.Validators.User
{
    public sealed class UpdateUserValidator : AbstractValidator<UpdateUserDTO>
    {
        public UpdateUserValidator()
        {
            When(
                u => u.Email != null,
                () => RuleFor(u => u.Email)
                    .EmailAddress()
                    .WithMessage("Email address must be valid")
            );

            When(
                u => u.FirstName != null,
                () => RuleFor(u => u.FirstName)
                    .NotEmpty()
                    .WithMessage("First name must not be empty")
            );

            When(
                u => u.LastName != null,
                () => RuleFor(u => u.LastName)
                    .NotEmpty()
                    .WithMessage("Last name must not be empty")
            );

            When(
                u => u.BirthDay != null,
                () => RuleFor(u => u.BirthDay)
                    .NotEmpty()
                    .LessThan(DateTime.UtcNow)
                    .WithMessage("Birth day must be earlier than now")
            );
        }
    }
}
