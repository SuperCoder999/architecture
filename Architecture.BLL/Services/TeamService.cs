using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using Architecture.DAL.Repositories;
using Architecture.DAL.Entities;
using Architecture.Common.DTO.Team;
using Architecture.BLL.Interfaces;

namespace Architecture.BLL.Sevices
{
    public sealed class TeamService : AbstractService<Team, TeamDTO, CreateTeamDTO, UpdateTeamDTO>, ITeamService
    {
        public TeamService(IRepository<Team> repository, IMapper mapper) : base(repository, mapper) { }

        public IEnumerable<TeamShortDTO> GetIdsNamesUsers()
        {
            IEnumerable<Team> teams = _repository.Get();

            return teams
                .Where(t => t.Users.All(u => DateTime.UtcNow.Year - u.BirthDay.Year > 10))
                .Select(t => new Team
                {
                    Id = t.Id,
                    Name = t.Name,
                    CreatedAt = t.CreatedAt,
                    Users = t.Users.OrderByDescending(u => u.RegisteredAt)
                })
                .Select(_mapper.Map<Team, TeamShortDTO>);
        }

        protected override Team UpdateDTOToFullEntity(int id, UpdateTeamDTO data)
        {
            Team currentEntity = _repository.Get(id);

            return new Team
            {
                Id = currentEntity.Id,
                Name = data.Name ?? currentEntity.Name,
                CreatedAt = currentEntity.CreatedAt,
            };
        }
    }
}
