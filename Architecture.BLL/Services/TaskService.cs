using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using Architecture.DAL.Repositories;
using Architecture.DAL.Entities;
using Architecture.Common.DTO.Task;
using Architecture.Common.Exceptions;
using Architecture.BLL.Interfaces;

namespace Architecture.BLL.Sevices
{
    public sealed class TaskService : AbstractService<TaskModel, TaskDTO, CreateTaskDTO, UpdateTaskDTO>, ITaskService
    {
        private readonly IRepository<User> _userRepository;

        public TaskService(
            IRepository<TaskModel> repository,
            IMapper mapper,
            IRepository<User> userRepository
        ) : base(repository, mapper)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<TaskDTO> GetAssignedToWithShortName(int userId)
        {
            if (!_userRepository.Exists(userId))
            {
                throw new NotFoundException("User");
            }

            IEnumerable<TaskModel> tasks = _repository.Get();

            return tasks
                .Where(t => t.Name.Length < 45 && t.PerformerId == userId)
                .Select(_mapper.Map<TaskModel, TaskDTO>);
        }

        public IEnumerable<TaskShortDTO> GetShortFinishedInCurrentYearAssignedTo(int userId)
        {
            if (!_userRepository.Exists(userId))
            {
                throw new NotFoundException("User");
            }

            IEnumerable<TaskModel> tasks = _repository.Get();

            return tasks
                .Where(t =>
                    t.FinishedAt.HasValue &&
                    t.FinishedAt.Value.Year == DateTime.UtcNow.Year &&
                    t.PerformerId == userId)
                .Select(_mapper.Map<TaskModel, TaskShortDTO>);
        }

        protected override TaskModel UpdateDTOToFullEntity(int id, UpdateTaskDTO data)
        {
            TaskModel currentEntity = _repository.Get(id);

            return new TaskModel
            {
                Id = currentEntity.Id,
                ProjectId = data.ProjectId.HasValue ? data.ProjectId.Value : currentEntity.ProjectId,
                PerformerId = data.PerformerId.HasValue ? data.PerformerId.Value : currentEntity.PerformerId,
                Name = data.Name ?? currentEntity.Name,
                Description = data.Description ?? currentEntity.Description,
                State = data.State.HasValue ? data.State.Value : currentEntity.State,
                CreatedAt = currentEntity.CreatedAt,
                FinishedAt = data.FinishedAt.HasValue ? data.FinishedAt : currentEntity.FinishedAt,
            };
        }
    }
}
