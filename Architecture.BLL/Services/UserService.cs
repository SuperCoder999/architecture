using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using Architecture.DAL.Repositories;
using Architecture.DAL.Entities;
using Architecture.Common.DTO.User;
using Architecture.BLL.Interfaces;

namespace Architecture.BLL.Sevices
{
    public sealed class UserService : AbstractService<User, UserDTO, CreateUserDTO, UpdateUserDTO>, IUserService
    {
        private readonly IRepository<TaskModel> _taskRepository;
        private readonly IRepository<Project> _projectRepository;

        public UserService(
            IRepository<User> repository,
            IMapper mapper,
            IRepository<TaskModel> taskRepository,
            IRepository<Project> projectRepository
        ) : base(repository, mapper)
        {
            _taskRepository = taskRepository;
            _projectRepository = projectRepository;
        }

        public IEnumerable<UserWithTasksDTO> GetWithTasksOrderedByFirstName()
        {
            IEnumerable<User> users = _repository.Get();
            IEnumerable<TaskModel> tasks = _taskRepository.Get();

            return users
                .GroupJoin<User, TaskModel, int, User>(
                    tasks,
                    u => u.Id,
                    t => t.PerformerId,
                    (u, tasks) => new User
                    {
                        Id = u.Id,
                        TeamId = u.TeamId,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Email = u.Email,
                        RegisteredAt = u.RegisteredAt,
                        BirthDay = u.BirthDay,
                        Tasks = tasks.OrderByDescending(t => t.Name.Length),
                    }
                )
                .Select(_mapper.Map<User, UserWithTasksDTO>)
                .OrderBy(u => u.FirstName);
        }

        public UserAdditionalInfoDTO GetAdditionalInfo(int userId)
        {
            User[] userAsArray = new User[] { _repository.Get(userId) };
            IEnumerable<Project> projects = _projectRepository.Get();
            IEnumerable<TaskModel> tasks = _taskRepository.Get();

            return userAsArray
                .GroupJoin<User, Project, int, UserAdditionalInfo>(
                    projects,
                    u => u.Id,
                    p => p.AuthorId,
                    (u, projects) =>
                    {
                        Project? lastProject = projects.OrderByDescending(p => p.CreatedAt).FirstOrDefault();

                        return new UserAdditionalInfo
                        {
                            User = u,
                            LastProject = lastProject,
                            LastProjectTasksCount = lastProject.HasValue ? lastProject.Value.Tasks.Count() : 0,
                        };
                    }
                )
                .GroupJoin<UserAdditionalInfo, TaskModel, int, UserAdditionalInfo>(
                    tasks,
                    info => info.User.Id,
                    t => t.PerformerId,
                    (info, tasks) =>
                    {
                        TaskModel? longestTask = tasks
                            .Where(t => t.FinishedAt.HasValue)
                            .OrderByDescending(t => t.FinishedAt!.Value - t.CreatedAt)
                            .FirstOrDefault();

                        return new UserAdditionalInfo
                        {
                            User = info.User,
                            LastProject = info.LastProject,
                            LastProjectTasksCount = info.LastProjectTasksCount,
                            UnfinishedTasksCount = tasks
                                .Where(t => !t.FinishedAt.HasValue)
                                .Count(),
                            LongestTask = longestTask,
                        };
                    }
                )
                .Select(_mapper.Map<UserAdditionalInfo, UserAdditionalInfoDTO>)
                .First();
        }

        protected override User UpdateDTOToFullEntity(int id, UpdateUserDTO data)
        {
            User currentEntity = _repository.Get(id);

            return new User
            {
                Id = currentEntity.Id,
                TeamId = data.TeamId.HasValue ? data.TeamId : currentEntity.TeamId,
                FirstName = data.FirstName ?? currentEntity.FirstName,
                LastName = data.LastName ?? currentEntity.LastName,
                Email = data.Email ?? currentEntity.Email,
                RegisteredAt = currentEntity.RegisteredAt,
                BirthDay = data.BirthDay.HasValue ? data.BirthDay.Value : currentEntity.BirthDay,
            };
        }
    }
}
