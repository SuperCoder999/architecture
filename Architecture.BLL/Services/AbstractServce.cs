using System.Collections.Generic;
using AutoMapper;
using Architecture.DAL.Entities;
using Architecture.DAL.Repositories;

namespace Architecture.BLL.Sevices
{
    public abstract class AbstractService<T, TDTO, TCreateDTO, TUpdateDTO>
        where T : IEntity
        where TDTO : struct
        where TCreateDTO : struct
        where TUpdateDTO : struct
    {
        protected readonly IRepository<T> _repository;
        protected readonly IMapper _mapper;

        public AbstractService(IRepository<T> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<TDTO> Get()
        {
            return _mapper.Map<IEnumerable<T>, IEnumerable<TDTO>>(_repository.Get());
        }

        public TDTO Get(int id)
        {
            return _mapper.Map<T, TDTO>(_repository.Get(id));
        }

        public TDTO Create(TCreateDTO data)
        {
            T entity = _repository.Create(_mapper.Map<TCreateDTO, T>(data));

            return _mapper.Map<T, TDTO>(entity);
        }

        public TDTO Update(int id, TUpdateDTO data)
        {
            T entity = UpdateDTOToFullEntity(id, data);
            T newEntity = _repository.Update(id, entity);

            return _mapper.Map<T, TDTO>(newEntity);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        protected abstract T UpdateDTOToFullEntity(int id, TUpdateDTO data);
    }
}
