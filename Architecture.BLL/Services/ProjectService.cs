using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using Architecture.DAL.Repositories;
using Architecture.DAL.Entities;
using Architecture.Common.DTO.Project;
using Architecture.Common.Exceptions;
using Architecture.BLL.Interfaces;

namespace Architecture.BLL.Sevices
{
    public sealed class ProjectService : AbstractService<Project, ProjectDTO, CreateProjectDTO, UpdateProjectDTO>, IProjectService
    {
        private readonly IRepository<User> _userRepository;

        public ProjectService(
            IRepository<Project> repository,
            IMapper mapper,
            IRepository<User> userRepository
        ) : base(repository, mapper)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<ProjectWithTasksCountDTO> WithTasksCountByUserId(int userId)
        {
            if (!_userRepository.Exists(userId))
            {
                throw new NotFoundException("User");
            }

            // Tasks count is handled by mapper (it is simple enough)

            return _repository.Get()
                .Where(p => p.AuthorId == userId)
                .Select(_mapper.Map<Project, ProjectWithTasksCountDTO>);
        }

        public ProjectAdditionalInfoDTO GetAdditionalInfo(int id)
        {
            Project[] projectAsArray = new Project[] { _repository.Get(id) };

            return projectAsArray
                .Select(p => new ProjectAdditionalInfo
                {
                    Project = p,
                    LongestDescriptionTask = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                    ShortestNameTask = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                    UsersCountInTeam =
                        p.Description.Length > 20 || p.Tasks.Count() < 3
                            ? p.Team.Users.Count()
                            : null
                })
                .Select(_mapper.Map<ProjectAdditionalInfo, ProjectAdditionalInfoDTO>)
                .First();
        }

        protected override Project UpdateDTOToFullEntity(int id, UpdateProjectDTO data)
        {
            Project currentEntity = _repository.Get(id);

            return new Project
            {
                Id = currentEntity.Id,
                AuthorId = data.AuthorId.HasValue ? data.AuthorId.Value : currentEntity.AuthorId,
                TeamId = data.TeamId.HasValue ? data.TeamId.Value : currentEntity.TeamId,
                Name = data.Name ?? currentEntity.Name,
                Description = data.Description ?? currentEntity.Description,
                Deadline = data.Deadline.HasValue ? data.Deadline.Value : currentEntity.Deadline,
                CreatedAt = currentEntity.CreatedAt,
            };
        }
    }
}
