using System.Collections.Generic;
using Architecture.Common.DTO.Task;

namespace Architecture.BLL.Interfaces
{
    public interface ITaskService : IServiceBase<TaskDTO, CreateTaskDTO, UpdateTaskDTO>
    {
        IEnumerable<TaskDTO> GetAssignedToWithShortName(int userId);
        IEnumerable<TaskShortDTO> GetShortFinishedInCurrentYearAssignedTo(int userId);
    }
}
