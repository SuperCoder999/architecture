using System.Collections.Generic;
using Architecture.Common.DTO.User;

namespace Architecture.BLL.Interfaces
{
    public interface IUserService : IServiceBase<UserDTO, CreateUserDTO, UpdateUserDTO>
    {
        IEnumerable<UserWithTasksDTO> GetWithTasksOrderedByFirstName();
        UserAdditionalInfoDTO GetAdditionalInfo(int userId);
    }
}
