using System.Collections.Generic;

namespace Architecture.BLL.Interfaces
{
    public interface IServiceBase<TDTO, TCreateDTO, TUpdateDTO>
        where TDTO : struct
        where TCreateDTO : struct
        where TUpdateDTO : struct
    {
        IEnumerable<TDTO> Get();
        TDTO Get(int id);
        TDTO Create(TCreateDTO data);
        TDTO Update(int id, TUpdateDTO data);
        void Delete(int id);
    }
}
