using System.Collections.Generic;
using Architecture.Common.DTO.Team;

namespace Architecture.BLL.Interfaces
{
    public interface ITeamService : IServiceBase<TeamDTO, CreateTeamDTO, UpdateTeamDTO>
    {
        IEnumerable<TeamShortDTO> GetIdsNamesUsers();
    }
}
