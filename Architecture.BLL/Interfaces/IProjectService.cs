using System.Collections.Generic;
using Architecture.Common.DTO.Project;

namespace Architecture.BLL.Interfaces
{
    public interface IProjectService : IServiceBase<ProjectDTO, CreateProjectDTO, UpdateProjectDTO>
    {
        IEnumerable<ProjectWithTasksCountDTO> WithTasksCountByUserId(int userId);
        ProjectAdditionalInfoDTO GetAdditionalInfo(int id);
    }
}
