using System;
using AutoMapper;
using Architecture.Common.DTO.Task;
using Architecture.DAL.Entities;

namespace Architecture.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<CreateTaskDTO, TaskModel>()
                .ForMember<DateTime>(t => t.CreatedAt, opt => opt.MapFrom(dto => DateTime.UtcNow));

            CreateMap<TaskModel, TaskDTO>();
            CreateMap<TaskModel, TaskWithoutPerformerDTO>();
            CreateMap<TaskModel, TaskShortDTO>();
        }
    }
}
