using System;
using AutoMapper;
using Architecture.Common.DTO.Team;
using Architecture.DAL.Entities;

namespace Architecture.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<CreateTeamDTO, Team>()
                .ForMember<DateTime>(t => t.CreatedAt, opt => opt.MapFrom(dto => DateTime.UtcNow));

            CreateMap<Team, TeamDTO>();
            CreateMap<Team, TeamShortDTO>();
        }
    }
}
