using System;
using System.Linq;
using AutoMapper;
using Architecture.DAL.Entities;
using Architecture.Common.DTO.Project;

namespace Architecture.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<CreateProjectDTO, Project>()
                .ForMember<DateTime>(p => p.CreatedAt, opt => opt.MapFrom(dto => DateTime.UtcNow));

            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectAdditionalInfo, ProjectAdditionalInfoDTO>();

            CreateMap<Project, ProjectWithTasksCountDTO>()
                .ForMember<int>(
                    dto => dto.TasksCount,
                    opt => opt.MapFrom(p => p.Tasks.Count())
                );
        }
    }
}
