using System;
using AutoMapper;
using Architecture.Common.DTO.User;
using Architecture.DAL.Entities;

namespace Architecture.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<CreateUserDTO, User>()
                .ForMember<DateTime>(u => u.RegisteredAt, opt => opt.MapFrom(dto => DateTime.UtcNow));

            CreateMap<User, UserDTO>();
            CreateMap<User, UserWithTasksDTO>();
            CreateMap<UserAdditionalInfo, UserAdditionalInfoDTO>();
        }
    }
}
