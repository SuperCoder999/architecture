using System;
using System.Linq;
using System.Collections.Generic;

namespace Architecture.Common.CommonExtensions
{
    public static class LinqExtensions
    {
        public static IEnumerable<T> ReplaceWhere<T, TKey>(
            this IEnumerable<T> collection,
            Predicate<T> isCorrect,
            T replacement
        )
        {
            return collection.Select(item => isCorrect(item) ? replacement : item);
        }

        public static IEnumerable<T> RemoveWhere<T, TKey>(
            this IEnumerable<T> collection,
            Predicate<T> isCorrect
        )
        {
            collection = collection.Where(item => !isCorrect(item));
            return collection;
        }
    }
}
