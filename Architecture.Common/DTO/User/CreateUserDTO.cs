using System;
using Newtonsoft.Json;

namespace Architecture.Common.DTO.User
{
    public struct CreateUserDTO
    {
        public int? TeamId { get; set; }
        [JsonRequired] public string FirstName { get; set; }
        [JsonRequired] public string LastName { get; set; }
        [JsonRequired] public string Email { get; set; }
        [JsonRequired] public DateTime BirthDay { get; set; }
    }
}
