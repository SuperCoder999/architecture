#nullable enable

using Architecture.Common.DTO.Project;
using Architecture.Common.DTO.Task;

namespace Architecture.Common.DTO.User
{
    public struct UserAdditionalInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO? LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public TaskDTO? LongestTask { get; set; }
    }
}
