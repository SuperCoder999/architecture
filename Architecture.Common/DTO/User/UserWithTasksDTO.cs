#nullable enable

using System;
using System.Collections.Generic;
using Architecture.Common.DTO.Task;

namespace Architecture.Common.DTO.User
{
    public struct UserWithTasksDTO
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public IEnumerable<TaskWithoutPerformerDTO> Tasks { get; set; }
    }
}
