using System.Collections.Generic;

namespace Architecture.Common.DTO
{
    public struct HttpErrorDTO
    {
        public Dictionary<string, IEnumerable<string>> Errors { get; set; }
    }
}
