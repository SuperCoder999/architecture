using System;
using System.Collections.Generic;
using Architecture.Common.DTO.User;

namespace Architecture.Common.DTO.Team
{
    public struct TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<UserDTO> Users;
    }
}
