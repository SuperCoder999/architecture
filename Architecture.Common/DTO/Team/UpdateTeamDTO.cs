#nullable enable

namespace Architecture.Common.DTO.Team
{
    public struct UpdateTeamDTO
    {
        public string? Name { get; set; }
    }
}
