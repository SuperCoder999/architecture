using System.Collections.Generic;
using Architecture.Common.DTO.User;

namespace Architecture.Common.DTO.Team
{
    public struct TeamShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDTO> Users;
    }
}
