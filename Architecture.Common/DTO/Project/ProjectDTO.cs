using System;
using System.Collections.Generic;
using Architecture.Common.DTO.Team;
using Architecture.Common.DTO.Task;
using Architecture.Common.DTO.User;

namespace Architecture.Common.DTO.Project
{
    public struct ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<TaskDTO> Tasks;
        public TeamDTO Team { get; set; }
        public UserDTO Author { get; set; }
    }
}
