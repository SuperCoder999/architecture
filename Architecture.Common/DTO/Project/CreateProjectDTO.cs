using System;
using Newtonsoft.Json;

namespace Architecture.Common.DTO.Project
{
    public struct CreateProjectDTO
    {
        [JsonRequired] public int AuthorId { get; set; }
        [JsonRequired] public int TeamId { get; set; }
        [JsonRequired] public string Name { get; set; }
        [JsonRequired] public string Description { get; set; }
        [JsonRequired] public DateTime Deadline { get; set; }
    }
}
