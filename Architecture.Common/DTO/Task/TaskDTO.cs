#nullable enable

using System;
using Architecture.Common.Enums;
using Architecture.Common.DTO.User;

namespace Architecture.Common.DTO.Task
{
    public struct TaskDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public UserDTO Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
