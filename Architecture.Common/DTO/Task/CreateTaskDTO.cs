
#nullable enable

using System;
using Architecture.Common.Enums;
using Newtonsoft.Json;

namespace Architecture.Common.DTO.Task
{
    public struct CreateTaskDTO
    {
        [JsonRequired] public int ProjectId { get; set; }
        [JsonRequired] public int PerformerId { get; set; }
        [JsonRequired] public string Name { get; set; }
        [JsonRequired] public string Description { get; set; }
        [JsonRequired] public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
