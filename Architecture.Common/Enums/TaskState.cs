namespace Architecture.Common.Enums
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Cancelled,
    }
}
